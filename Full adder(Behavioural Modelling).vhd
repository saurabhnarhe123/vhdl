----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:33:52 09/26/2016 
-- Design Name: 
-- Module Name:    add1 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity add1 is
    Port ( a : in  STD_LOGIC;
           b : in  STD_LOGIC;
           c : in  STD_LOGIC;
           s : out  STD_LOGIC;
           cr : out  STD_LOGIC);
end add1;

architecture Behavioral of add1 is

begin

process (a, b, c)
begin 

if(a='0' and b='0' and c='0') then 
		s <= '0';
		cr <= '0';
	elsif(a='0' and b='0' and c='1') then 
		s <= '1';
		cr <= '0';
		elsif(a='0' and b='1' and c='0') then 
			s <= '1';
			cr <= '0';
			elsif(a='0' and b='1' and c='1') then 
				s <= '0';
				cr <= '1';
				elsif(a='1' and b='0' and c='0') then 
					s <= '1';
					cr <= '0';
					elsif(a='1' and b='0' and c='1') then 
						s <= '0';
						cr <= '1';
						elsif(a='1' and b='1' and c='0') then 
							s <= '0';
							cr <= '1';
							elsif(a='1' and b='1' and c='1') then 
								s <= '1';
								cr <= '1';

end if;

end process;

end Behavioral;
