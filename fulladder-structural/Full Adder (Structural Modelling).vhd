----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:22:58 09/26/2016 
-- Design Name: 
-- Module Name:    Full_Adder_Str - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: Designing of full adder using 2 half adders 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Full_Adder_Str is
    Port ( a : in  STD_LOGIC;
           b : in  STD_LOGIC;
           c : in  STD_LOGIC;
           sum : out  STD_LOGIC;
           carry : out  STD_LOGIC);
end Full_Adder_Str;

architecture Behavioral of Full_Adder_Str is

component halfadd 
port( a : in STD_LOGIC;
		b : in STD_LOGIC;
		carry : out STD_LOGIC;
		sum : out STD_LOGIC );
end component;

signal sum1, carry1, carry2 : STD_LOGIC;

begin

halfadder1 : halfadd port map (a => a, b => b, carry => carry1, sum => sum1);
halfadder2 : halfadd port map (a => sum1, b => c, carry => carry2, sum => sum);

carry <= carry1 OR carry2;

end Behavioral;
