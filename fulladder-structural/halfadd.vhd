----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    19:09:10 09/22/2016 
-- Design Name: 
-- Module Name:    half - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: Half adder for full adder design
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity half is
    Port ( a,b : in  STD_LOGIC;
           sum,carry : out  STD_LOGIC);
end half;

architecture Behavioral of half is
sum<=a XOR b;
carry<=a and b;

end Behavioral;
